from datetime import datetime
from typing import Union

from django.db import models
from django.utils.translation import gettext_lazy as _

from base.models import BaseModel

StatusMessage = (
    (0, _("Error")),
    (1, _("Success")),
)

StatusMailing = (
    (0, _("is not started")),
    (1, _("processed")),
    (2, _("done")),
    (3, _("error"))
)


class TagModel(BaseModel):
    name: str = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Tag"
        verbose_name_plural = "Tags"

    def __str__(self):
        return f"id: {self.pk} | name: {self.name}"


class MobileOperatorModel(BaseModel):
    operator: str = models.CharField(max_length=255)
    code: str = models.CharField(max_length=7)

    class Meta:
        verbose_name = "Mobile Operator"
        verbose_name_plural = "Mobile Operators"

    def __str__(self) -> str:
        return f"id: {self.pk} | code operator: {self.code}"


class MailingModel(BaseModel):
    text: str = models.TextField()
    operator: Union[MobileOperatorModel, dict, int, None] = models.ForeignKey(
        MobileOperatorModel, related_name="mailing_mobile_operator", on_delete=models.SET_NULL, null=True)
    start_datetime: datetime = models.DateTimeField()
    end_datetime: datetime = models.DateTimeField()
    tag: Union[TagModel, dict, int, None] = models.ForeignKey(
        TagModel, related_name="tag_mailing", on_delete=models.SET_NULL, null=True)
    status: int = models.IntegerField(choices=StatusMailing, default=0)
    error_message = models.TextField(null=True)

    class Meta:
        verbose_name: str = "Mailing"
        verbose_name_plural: str = "Mailings"

    def __str__(self) -> str:
        return f"id: {self.pk} | start datetime: {self.start_datetime}"

    def save(self, *args, **kwargs):
        status = kwargs.pop("status", None)
        error_message = kwargs.pop("error_message", None)
        if status is not None and type(status) is int:
            self.status = status
        elif error_message is not None and type(error_message) is str:
            self.error_message = error_message
        return super(MailingModel, self).save(*args, **kwargs)


class ClientModel(BaseModel):
    operator: Union[MobileOperatorModel, dict, int, None] = models.ForeignKey(
        MobileOperatorModel, related_name="client_mobile_operator", on_delete=models.SET_NULL, null=True)
    phone = models.CharField(max_length=13)
    tag: Union[TagModel, dict, int, None] = models.ForeignKey(
        TagModel, related_name="tag_client", on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = "Client"
        verbose_name_plural = "Clients"

    def __str__(self) -> str:
        return f"id: {self.pk} | phone: {self.phone}"


class StatusSendMessageModel(BaseModel):
    mailing: Union[MailingModel, dict, int, None] = models.ForeignKey(
        MailingModel, related_name="status_mailing", on_delete=models.SET_NULL, null=True)
    user: Union[ClientModel, dict, int, None] = models.ForeignKey(
        ClientModel, related_name="client", on_delete=models.SET_NULL, null=True)
    status = models.IntegerField(choices=StatusMessage, default=1)  # tag

    class Meta:
        verbose_name = "Status Message"
        verbose_name_plural = "Status Messages"

    def __str__(self):
        return f"id: {self.pk} | status: {self.status}"
