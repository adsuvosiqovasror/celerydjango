from typing import Union

import requests
from django.db.models import QuerySet, Q
from django.http import HttpRequest
from ninja import Router, Query

from base.ORM import AllCustomRequest
from base.utils import update, update_data, check_paginate, remove_null_from_Q_object
from main import schemes

from main.models import MobileOperatorModel, MailingModel, ClientModel, TagModel
from settings.settings import PROBE_SERVER_TOKEN

app = Router()
orm = AllCustomRequest


@app.post("/operators", response=schemes.MobileOperatorSchemeOut, tags=["operator"], exclude_unset=True)
def create_operator(
        request: HttpRequest,
        payload: schemes.MobileOperatorSchemeIn
):
    payload = payload.dict()
    return MobileOperatorModel.objects.create(**payload)


@app.get("/operators", response=list[schemes.MobileOperatorSchemeOut], tags=["operator"], exclude_unset=True)
def get_operators(
        request: HttpRequest,
        page: Union[int, None] = Query(default=None, ge=1),
        code: Union[str, None] = Query(
            default=None, description="example: input: '7' output: +7, +374, +375"),
):
    page_size: int = 15
    if page is None and code is None:
        data: list = list(orm(MobileOperatorModel, end=page_size).limit_all)
        data.append({"count": MobileOperatorModel.objects.count()})
        return data
    elif code is not None and page is None:
        data: QuerySet = orm(MobileOperatorModel, fd_kwargs={"code__icontains": code}, end=page_size).limit_filter
        return update_data(data)
    elif code is not None and page is not None:
        start = page_size * page
        end = start + page_size
        data: QuerySet = orm(MobileOperatorModel, fd_kwargs={"code__icontains": code}, start=start,
                             end=end).paginate_filter
        return update_data(data)
    start: int = page_size * page
    end: int = start + page_size
    data: QuerySet = orm(MobileOperatorModel, start=start, end=end).paginate_all
    return update_data(data)


@app.get("/operator/{operator_id}", response=schemes.MobileOperatorSchemeOut, tags=["operator"], exclude_unset=True)
def get_operator(
        request: HttpRequest,
        operator_id: int
):
    return orm(MobileOperatorModel, fd_kwargs={"pk": operator_id}).get


@app.delete("/operator/{operator_id}", response=schemes.Message, tags=["operator"], exclude_unset=True)
def delete_operator(
        request: HttpRequest,
        operator_id: int
):
    obj = orm(MobileOperatorModel, fd_kwargs={"pk": operator_id}).get
    obj.delete()
    return {"message": "success deleted"}


@app.patch("/operator/{operator_id}", response=schemes.MobileOperatorSchemeOut, tags=["operator"], exclude_unset=True)
def update_operator(
        request: HttpRequest,
        operator_id: int,
        payload: schemes.MobileOperatorSchemeIn
):
    obj = orm(MobileOperatorModel, fd_kwargs={"pk": operator_id}).get
    return update(obj, payload)


@app.get("/mailing/{mailing_id}", response=schemes.MailingSchemeOut, tags=["mailing"], exclude_unset=True)
def get_mailing(
        request: HttpRequest,
        mailing_id: int
):
    return orm(MailingModel, fd_kwargs={"pk": mailing_id}).get


@app.post("/mailing", response=schemes.MailingSchemeOut, tags=["mailing"], exclude_unset=True)
def create_mailing(
        request: HttpRequest,
        payload: schemes.MailingSchemeIn,

):
    payload = payload.dict(exclude_none=True)
    return MailingModel.objects.create(**payload)


@app.get("/mailing", response=list[schemes.MailingSchemeOut], tags=["mailing"], exclude_unset=True)
def get_mailings(
        request: HttpRequest,
        page: Union[int, None] = Query(default=None),
        status: Union[int, None] = Query(
            default=None, ge=0, le=4, description="0: не начато, 1: в процессе, 2: успешно выполнен, 3: ошибка")
):
    filter_data: dict = dict()
    if status is not None and type(status) is int and 0 <= status <= 4:
        filter_data["status"] = status
    print(filter_data)
    return check_paginate(MailingModel, page, filter_data)


@app.delete("/mailing/{mailing_id}", response=schemes.Message, tags=["mailing"], exclude_unset=True)
def delete_mailing(
        request: HttpRequest,
        mailing_id: int
):
    obj = orm(MailingModel, fd_kwargs={"pk": mailing_id}).get
    obj.delete()
    return {"message": "success deleted"}


@app.patch("/mailing/{mailing_id}", response=schemes.MailingSchemeOut, tags=["mailing"], exclude_unset=True)
def update_mailing(
        request: HttpRequest,
        mailing_id: int,
        payload: schemes.MailingSchemeOut
):
    obj = orm(MailingModel, fd_kwargs={"pk": mailing_id}).get
    payload = payload.dict(exclude_none=True)
    operator_id = payload.get("operator")
    if operator_id is not None:
        payload["operator_id"] = operator_id
    return update(obj, payload)


@app.post("/client", response=schemes.ClientSchemeOut, tags=["client"], exclude_unset=True)
def create_client(
        request: HttpRequest,
        payload: schemes.ClientSchemeIn
):
    payload = payload.dict()
    operator_id = payload.pop("operator")
    return ClientModel.objects.create(operator_id=operator_id, **payload)


@app.get("/client", response=list[schemes.ClientSchemeOut], tags=["client"], exclude_unset=True)
def get_clients(
        request: HttpRequest,
        page: Union[int, None] = Query(default=None),
        operator_id: Union[int, None] = Query(alias="operator", default=None, description="operator id"),
        tag_id: Union[int, None] = Query(alias="tag", default=None, description="tag id"),
        operator_code: Union[str, None] = Query(
            default=None, description="example: input: '7' output: +7 ..., +374 ..., +375 ..."),
        search: bool = Query(
            default=False, description="если стоавить True то будет не фильровать а будет искать по вашим параметрам")
):
    if search:
        args = (Q(operator_id=operator_id) | Q(tag_id=tag_id) | Q(operator__code__icontains=operator_code))
        filter_data_args, q_filtered = remove_null_from_Q_object(args)
        return check_paginate(ClientModel, filter_data_args=filter_data_args, page=page, q_filtered=q_filtered)
    filter_data_kwargs = schemes.ClientQueryFilterScheme(
        operator_id=operator_id, tag_id=tag_id, operator_code=operator_code)
    return check_paginate(ClientModel, filter_data_kwargs=filter_data_kwargs.dict(exclude_none=True), page=page)


@app.get("/client/{client_id}", response=schemes.ClientSchemeOut, tags=["client"], exclude_unset=True)
def get_client(
        request: HttpRequest,
        client_id: int
):
    return orm(ClientModel, fd_kwargs={"pk": client_id}).get


@app.delete("/client/{client_id}", response=schemes.Message, tags=["client"], exclude_unset=True)
def delete_client(
        request: HttpRequest,
        client_id: int
):
    obj = orm(ClientModel, fd_kwargs={"pk": client_id}).get
    obj.delete()
    return {"message": "success deleted"}


@app.patch("/client/{client_id}", response=schemes.ClientSchemeOut, tags=["client"], exclude_unset=True)
def update_client(
        request: HttpRequest,
        client_id: int,
        payload: schemes.ClientSchemeIn
):
    obj = orm(ClientModel, fd_kwargs={"pk": client_id}).get
    payload = payload.dict(exclude_none=True)
    payload["client_id"] = payload.pop("operator")
    return update(obj, payload)


@app.post("/tag", response=schemes.TagSchemeOut, tags=["tag"], exclude_unset=True)
def create_tag(
        request: HttpRequest,
        payload: schemes.TagSchemeIn
):
    payload = payload.dict()
    return TagModel.objects.create(**payload)


@app.get("/tag", response=list[schemes.TagSchemeOut], tags=["tag"], exclude_unset=True)
def get_tags(
        request: HttpRequest,
):
    return orm(TagModel).all


@app.get("/tag/{tag_id}", response=schemes.TagSchemeOut, tags=["tag"], exclude_unset=True)
def get_tag(
        request: HttpRequest,
        tag_id: int
):
    return orm(TagModel, fd_kwargs={"pk": tag_id}).get


@app.delete("/tag/{tag_id}", response=schemes.Message, tags=["tag"], exclude_unset=True)
def delete_tag(
        request: HttpRequest,
        tag_id: int
):
    obj = orm(TagModel, fd_kwargs={"pk": tag_id}).get
    obj.delete()
    return {"message": "success deleted"}


@app.patch("/tag/{tag_id}", response=schemes.TagSchemeOut, tags=["tag"], exclude_unset=True)
def update_tag(
        request: HttpRequest,
        tag_id: int,
        payload: schemes.TagSchemeIn
):
    obj = orm(TagModel, fd_kwargs={"pk": tag_id}).get
    return update(obj, payload)


@app.post(
    "/probe_server",
    response=schemes.Message,
    tags=["Probe Server"]
)
def probe_server(
        request: HttpRequest,
        payload: schemes.ProbeServerSchemeIn,
        id: int = Query(description="ID отправляемого сообщения", default=None)
):
    url = f'https://probe.fbrq.cloud/v1/send/{id}'
    token = PROBE_SERVER_TOKEN
    if payload.token is not None:
        token = payload.token
    headers = {"Authorization": f"Bearer {token}"}
    data = payload.json(exclude_none=True)
    r = requests.post(url, data=data, headers=headers)
    if r.status_code == 200:
        return {"message": "success"}
    return {"message": r.content}
