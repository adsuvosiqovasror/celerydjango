import re

from datetime import datetime
from typing import Union

from ninja import Schema
from ninja.errors import HttpError
from pydantic import Field, validator, root_validator, BaseModel

from base.ORM import AllCustomRequest

from main.models import MobileOperatorModel

orm = AllCustomRequest


# class BaseScheme(Schema):
#     created_at: datetime, None = None
#     updated_at: datetime, None = None


class Message(Schema):
    message: str


class MobileOperatorSchemeIn(Schema):
    operator: str = Field(max_length=255, default=None, alias="operator_name")
    code: str = Field(max_length=6, default=None)

    @validator("code", allow_reuse=True)
    def check_code(cls, v: str):
        if "+" != v[0] or len(v) < 2:
            raise HttpError(status_code=400, message="error code \nexample: +998")
        try:
            [int(x) for x in v[1:]]
        except ValueError:
            raise HttpError(status_code=400, message="error code")
        operator = orm(MobileOperatorModel, fd_kwargs={"code": v}).filter
        if operator.exists():
            raise HttpError(status_code=400, message="such operator code already exists")
        return v


class MobileOperatorSchemeOut(Schema):
    id: Union[int, None] = None
    operator_name: Union[str, None] = Field(max_length=255, default=None, alias="operator")
    code: Union[str, None] = Field(max_length=6, default=None)
    count: Union[int, None] = None
    created_at: Union[datetime, None] = None


# class PaginateMobileOperatorScheme(BaseModel):
#     count: int


class TagSchemeIn(Schema):
    name: str = Field(alias="tag_name")


class TagSchemeOut(Schema):
    id: int
    name: str


class MailingSchemeIn(Schema):
    text: str
    start_datetime: datetime
    end_datetime: datetime
    tag_id: Union[int, None] = Field(default=None, alias="tag")
    operator_id: Union[int, None] = Field(default=None, alias="operator")


class MailingSchemeOut(Schema):
    id: Union[int, None] = None
    count: Union[int, None] = None
    status: Union[int, None] = None
    operator: Union[MobileOperatorSchemeOut, dict, None] = Field(default=None)
    start_datetime: Union[datetime, None] = None
    end_datetime: Union[datetime, None] = None
    text: Union[str, None] = None
    error_message: Union[str, None] = None
    tag: Union[TagSchemeOut, dict, int, None] = None


class ClientSchemeIn(Schema):
    operator: Union[int, dict, MobileOperatorSchemeOut]
    phone: str = Field(max_length=20)
    tag_id: int = Field(alias="tag")

    @root_validator  # https://pydantic-docs.helpmanual.io/usage/validators/#root-validators
    def validate_phone(cls, values):
        """
        В документации можно прочитать, зачем надо проверить на существование перемнного (attr is not None)
        """
        operator, phone = values.get('operator'), values.get('phone')
        if operator is not None and phone is not None:
            if type(operator) is not int or type(phone) is not str:
                raise HttpError(status_code=400, message="bad request type, attr operator not integer")
            operator = orm(MobileOperatorModel, fd_kwargs={"pk": operator}).get
            if operator.code not in phone:
                raise HttpError(status_code=400, message="operator and number code do not match")
        return values

    @validator("phone")
    def update_phone(cls, v):
        phone = re.sub("[^0-9+]", "", v)
        return phone


class ClientSchemeOut(Schema):
    id: Union[int, None] = None
    count: Union[int, None] = None
    operator: Union[MobileOperatorSchemeOut, dict, None] = None
    phone: Union[str, None] = None
    tag: Union[TagSchemeOut, None] = None


class StatusSendMessageSchemeIn(Schema):
    mailing: Union[MailingSchemeOut, dict, int, None] = None
    user: Union[ClientSchemeOut, dict, int, None] = None
    status: int = Field(ge=0, le=1)


class StatusSendMessageSchemeOut(Schema):
    id: Union[int, None] = None
    mailing: Union[MailingSchemeOut, dict, int, None] = None
    user: Union[ClientSchemeOut, dict, int, None] = None
    status: Union[int, None] = Field(ge=0, le=1, default=None)


class ClientQueryFilterScheme(BaseModel):
    operator_id: Union[int, None] = None
    tag_id: Union[int, None] = None
    operator__code__icontains: Union[str, None] = Field(default=None, alias="operator_code")


class ProbeServerSchemeIn(BaseModel):
    id: int
    phone: int
    text: str
    token: Union[str, None] = Field(description="вставьте токен авторизации если тот токен поумолчанию устарел")


class ProbeServerSchemeOut(BaseModel):
    code: int
    message: str
