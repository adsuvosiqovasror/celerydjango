from django.db.models import QuerySet
from django.utils import timezone

from ninja.errors import HttpError

from main import schemes
from settings.celery import app
from main.models import MailingModel, ClientModel, StatusSendMessageModel, TagModel


def mailing(spam_email: QuerySet):
    for obj in spam_email:
        print(timezone.now())
        try:
            filter_data = schemes.ClientQueryFilterScheme(operator_id=obj.operator_id, tag_id=obj.tag_id)
            spam_message_users = ClientModel.objects.filter(**filter_data.dict(exclude_none=True))
            print(obj.start_datetime > timezone.now())
            if obj.start_datetime < timezone.now() < obj.end_datetime:
                print("started", obj)
                obj.save(status=1)
                for user in spam_message_users:
                    print(obj.start_datetime, timezone.now())
                    if timezone.now() > obj.end_datetime:
                        StatusSendMessageModel(user_id=user.id, mailing_id=obj.id, status=0).save()
                        continue
                    StatusSendMessageModel(user_id=user.id, mailing_id=obj.id).save()
            elif obj.start_datetime > timezone.now():
                print("no started", obj)
                continue
            else:
                print("error", obj)
                for user in spam_message_users:
                    StatusSendMessageModel(user_id=user.id, mailing_id=obj.id, status=0).save()
                raise HttpError(
                    status_code=400,
                    message=f"время рассылки для старта или для окончания, меньше чем настоящее время: {timezone.now()}")
        except Exception as exc:
            obj.save(error_message=exc, status=3)
        else:
            print("success")
            obj.save(status=2)


@app.task
def check_spam_email():
    spam_email = MailingModel.objects.filter(status=0)
    if spam_email.exists():
        print(spam_email)
        mailing(spam_email)
