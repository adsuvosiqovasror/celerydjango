from typing import Union

from django.db.models import Q
from django.http import HttpRequest
from ninja import Router, Query

from base.utils import remove_null_from_Q_object, check_paginate
from dashboard import schemes

from main.models import MailingModel, StatusSendMessageModel
from main import schemes as main_schemes

from base.ORM import AllCustomRequest

app = Router()
orm = AllCustomRequest


@app.get(
    "/",
    tags=["dashboard"],
    response=schemes.DashboardScheme
)
def get_statistic_mailing(request: HttpRequest):
    """
    all_mailing: кол-во всех заплонированных рассылок\n
    count_mailing_users: кол-во всех пользователей рассыланых на них рассылок\n
    count_success_mailing: кол-во всех успешных рассылок\n
    count_failed_mailing: кол-во всех не успешных рассылок\n
    count_processed_mailing: кол-во всех рассылок которые находятся в просессе рассылки\n
    count_not_started_mailing: кол-во всех не запушенных рассылок\n
    count_users_success_mailing: кол-во всех пользователей успешно получивших рассылку\n
    count_users_failed_mailing: кол-во всех пользователей не успешно получивших рассылку\n
    count_users_processed_mailing: кол-во всех пользователей которые находятся в просессе рассылки\n
    avg_success_mailing: среднее кол-во всех успешных рассылок\n
    avg_failed_mailing: среднее кол-во всех не успешных рассылок\n
    avg_processed_mailing: среднее кол-во всех рассылок которые нходятся в процессе рассылки\n
    avg_users_success_mailing: среднее кол-во всех пользователей успешно получивших рассылку\n
    avg_users_failed_mailing: среднее кол-во всех пользователей не успешно получивших рассылку\n
    avg_users_processed_mailing: среднее кол-во всех пользователей которые находятся в просессе рассылки\n
    """
    data: dict = dict()
    data["all_mailing"] = MailingModel.objects.all().count()
    data["count_success_mailing"] = orm(MailingModel, fd_kwargs={"status": 2}).filtered_count
    data["count_failed_mailing"] = orm(MailingModel, fd_kwargs={"status": 3}).filtered_count
    data["count_processed_mailing"] = orm(MailingModel, fd_kwargs={"status": 1}).filtered_count
    data["count_not_started_mailing"] = orm(MailingModel, fd_kwargs={"status": 0}).filtered_count
    data["count_mailing_users"] = StatusSendMessageModel.objects.all().count()
    data["count_users_success_mailing"] = orm(
        StatusSendMessageModel, fd_args=(Q(status=1) | Q(mailing__status=2) & ~Q(status=0)),
        q_filtered=True).filtered_count
    data["count_users_failed_mailing"] = orm(
        StatusSendMessageModel, fd_args=(Q(status=0) | (Q(mailing__status=3) & ~Q(status=1))),
        q_filtered=True).filtered_count
    data["count_users_processed_mailing"] = orm(StatusSendMessageModel, fd_kwargs={"mailing__status": 1}).filtered_count
    for key, value in data.items():
        if value == 0:
            return data
    data["avg_success_mailing"] = data["all_mailing"] / data["count_success_mailing"]
    data["avg_failed_mailing"] = data["all_mailing"] / data["count_failed_mailing"]
    data["avg_processed_mailing"] = data["all_mailing"] / data["count_processed_mailing"]
    data["avg_not_started_mailing"] = data["all_mailing"] / data["count_not_started_mailing"]
    data["avg_users_success_mailing"] = data["count_mailing_users"] / data["count_users_success_mailing"]
    data["avg_users_failed_mailing"] = data["count_mailing_users"] / data["count_users_failed_mailing"]
    data["avg_users_processed_mailing"] = data["count_mailing_users"] / data["count_users_processed_mailing"]
    return data


@app.get(
    "/mailing",
    tags=["dashboard"],
    response=list[main_schemes.MailingSchemeOut],
    exclude_unset=True
)
def get_mailing(
        request: HttpRequest,
        page: Union[int, None] = Query(default=None, ge=1),
        status: Union[int, None] = Query(
            default=None, ge=0, le=4, description="0: не начато, 1: в процессе, 2: успешно выполнен, 3: ошибка"),
        text: Union[str, None] = Query(
            default=None, description="текс для фильтрации", max_length=40),
        error_message: Union[str, None] = Query(
            default=None, description="текс ошибки для фильтрации", max_length=40),
        operator_code: Union[str, None] = Query(
            default=None, alias="operator", description="введите код оператора", max_length=5),
        search: bool = Query(
            default=False, description="если стоавить True то будет не фильровать а будет искать по вашим параметрам")
):
    if search:
        args = (Q(status=status) | Q(text__icontains=text) | Q(
            error_message__icontains=error_message) | Q(operator__code__icontains=operator_code))
        filter_data_args, q_filtered = remove_null_from_Q_object(args)
        return check_paginate(MailingModel, filter_data_args=filter_data_args, page=page, q_filtered=q_filtered)
    query_data = schemes.FilterQueryMailingScheme(
        status=status, text=text, error_message=error_message, operator_code=operator_code)
    return check_paginate(MailingModel, filter_data_kwargs=query_data.dict(exclude_none=True), page=page)


@app.get(
    "/client",
    tags=["dashboard"],
    response=list[main_schemes.StatusSendMessageSchemeOut],
    exclude_unset=True
)
def get_client(
        request: HttpRequest,
        page: Union[int, None] = Query(default=None, ge=1),
        mailing_text: Union[str, None] = Query(
            default=None, description="часть текста написанный для spam рассылки", max_length=30),
        mailing_operator_code: Union[str, None] = Query(
            default=None, description="код оператора который выбран для spam рассылки", max_length=5),
        mailing_operator_name: Union[str, None] = Query(
            default=None, description="имя оператора который выбран для spam рассылки", max_length=30),
        mailing_status: Union[int, None] = Query(default=None,
                                                 description="статус spam рассылки, пример: 0: не начато, 1: в процессе, 2: успешно выполнен, 3: ошибка",
                                                 ge=0, le=4),
        mailing_error_message: Union[str, None] = Query(
            default=None, description="часть текста из ошибки в spam рассылки", max_length=30),
        user_operator_name: Union[str, None] = Query(
            default=None, description="имя оператора пользователя(client) spam рассылки", max_length=25),
        user_operator_code: Union[str, None] = Query(
            default=None, description="код(+7 or +998 ...) оператора пользователя(client) spam рассылки", max_length=5),
        user_tag_name: Union[str, None] = Query(
            default=None, description="имя тега который связан пользователем", max_length=30),
        user_phone: Union[str, None] = Query(default=None, description="номер или часть номера получателя(client)"),
        status: Union[int, None] = Query(
            default=None, description="статус полученный spam рассылок", ge=0, le=1),
        search: bool = Query(
            default=False, description="если стоавить True то будет не фильровать а будет искать по вашим параметрам")
):
    if search:
        args = (Q(mailing__text__icontains=mailing_text) | Q(mailing__operator__code__icontains=mailing_operator_code) |
                Q(mailing__operator__name__icontains=mailing_operator_name) | Q(mailing__status=mailing_status) |
                Q(mailing__error_message__icontains=mailing_error_message) |
                Q(user__operator__name__icontains=user_operator_name) |
                Q(user__operator__code__icontains=user_operator_code) | Q(user__tag__name__icontains=user_tag_name) |
                Q(user__phone__icontains=user_phone) | Q(status=status))
        filter_data_args, q_filtered = remove_null_from_Q_object(args)
        return check_paginate(StatusSendMessageModel, filter_data_args=filter_data_args, page=page,
                              q_filtered=q_filtered)
    query_data = schemes.FilterQueryClientScheme(
        mailing_text=mailing_text, mailing_operator_code=mailing_operator_code,
        mailing_operator_name=mailing_operator_name, mailing_status=mailing_status,
        mailing_error_message=mailing_error_message, user_operator_name=user_operator_name,
        user_operator_code=user_operator_code, user_tag_name=user_tag_name, user_phone=user_phone, status=status)
    return check_paginate(StatusSendMessageModel, filter_data_kwargs=query_data.dict(exclude_none=True), page=page)
