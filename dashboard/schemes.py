from typing import Union

from pydantic import BaseModel, Field, root_validator


class DashboardScheme(BaseModel):
    all_mailing: int
    count_mailing_users: int
    count_success_mailing: int
    count_failed_mailing: int
    count_processed_mailing: int
    count_not_started_mailing: int
    count_users_success_mailing: int
    count_users_failed_mailing: int
    count_users_processed_mailing: int
    avg_success_mailing: int = 0
    avg_failed_mailing: int = 0
    avg_processed_mailing: int = 0
    avg_not_started_mailing: int = 0
    avg_users_success_mailing: int = 0
    avg_users_failed_mailing: int = 0
    avg_users_processed_mailing: int = 0


class FilterQueryMailingScheme(BaseModel):
    status: Union[int, None] = Field(default=None, ge=0, le=4)
    text__icontains: Union[str, None] = Field(default=None, max_length=40, alias="text")
    error_message__icontains: Union[str, None] = Field(default=None, max_length=40, alias="error_message")
    operator__code__icontains: Union[str, None] = Field(default=None, max_length=10, alias="operator_code")


class FilterQueryClientScheme(BaseModel):
    mailing__text__icontains: Union[str, None] = Field(
        default=None, max_length=30, alias="mailing_text")
    mailing__operator__code__icontains: Union[str, None] = Field(
        default=None, max_length=5, alias="mailing_operator_code")
    mailing__operator__name__icontains: Union[str, None] = Field(
        default=None, max_length=30, alias="mailing_operator_name")
    mailing__status: Union[int, None] = Field(
        default=None, ge=0, le=4, alias="mailing_status")
    mailing__error_message__icontains: Union[str, None] = Field(
        default=None, max_length=30, alias="mailing_error_message")
    user__operator__name__icontains: Union[str, None] = Field(
        default=None, max_length=25, alias="user_operator_name")
    user__operator__code__icontains: Union[str, None] = Field(
        default=None, max_length=5, alias="user_operator_code")
    user__tag__name__icontains: Union[str, None] = Field(
        default=None, max_length=30, alias="user_tag_name")
    user__phone__icontains: Union[str, None] = Field(
        default=None, max_length=13, alias="user_phone")
    status: Union[int, None] = Field(
        default=None, ge=0, le=1, alias="status")
