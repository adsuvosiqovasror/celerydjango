from ninja import NinjaAPI
from main.api import app as example
from dashboard.api import app as dashboard

api = NinjaAPI(title="Test", version="1.0.0", description="TEST API")  # , csrf=True)

api.add_router("/", example)
api.add_router("/dashboard", dashboard)
